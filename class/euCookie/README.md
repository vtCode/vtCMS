This is the way I do this on vtCMX 1.0.0

1) Do this first:
=================
 * got this from:  www.cookiegaurd.eu
 * create a folder to put *.js files into. I called mein "euCookie/js".
 * get the cookie info you need to use on from that site, YOU NEED THIS INFO FOR "part2" below.
 * remove the first 2 lines from the script they give you.
   ** These 2 lines are something like:
    <script src="jquery.min.js"></script>
    <script src="jqueryCookieGuard.1.0.min.js>"></script>


2) Change those 2 lines to something simular to below. (Change "euCookie/js" to fit yours if needed.)
   And then add those lines within <head> & </head>:    
   NOTE: if you are using the same folder as I did, then you can copy and paste that)
======================================================================================================

<!-- COOKIEGAURD PART1 START -->
    <script type="text/javascript" src="<{xoAppUrl class/euCookie/js/jquery.min.js}>"></script>
    <script type="text/javascript" src="<{xoAppUrl class/euCookie/js/jqueryCookieGuard.1.0.min.js}>"></script>
<!-- COOKIEGAURD PART1 END -->


2) Add these lines within <body></body> add:
=============================================

<!-- COOKIEGAURD PART2 START -->
    <script>
      $(document).ready(function(){
        $.cookieguard();
        $.cookieguard.cookies.add('Facebook', 'datr,lu,c_user,xs', 'Facebook uses cookies to track the sites you visit', false);
        $.cookieguard.cookies.add('Twitter', 'guest_id,k,pid,twitter_sess', 'Twitter uses cookies to maintain widgets', false);
        $.cookieguard.cookies.add('PHP Session', 'PHPSESSID', 'This cookie is used to track important logical information for the smooth operation of the site', true);
        $.cookieguard.cookies.add('Google Analytics', '__utma,__utmb,__utmc,__utmz,__utmv', 'These cookies are used to collect information about how visitors use our site. We use the information to compile reports and to help us improve the site. The cookies collect information in an anonymous form, including the number of visitors to the site, where visitors have come to the site from and the pages they visited.', false);
        $.cookieguard.run();
      });
    </script>
<!-- COOKIEGAURD PART2 END -->


