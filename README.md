<u><b>What is it?</b></u>
<p>
<b>vtCMS</b> is a dynamic OO (Object Oriented) based open source portal script written in PHP.
vtCMS supports a number of databases,
making vtCMS an ideal tool for developing small to large dynamic community websites,
intra company portals, corporate portals, weblogs and much more.
</p>
<p>
vtCMS is released under the terms of the <a href='http://www.gnu.org/copyleft/gpl.html' target='_blank'>GNU General Public License (GPL)</a> and is free to use and modify.
It is free to redistribute as long as you abide by the distribution terms of the GPL.
</p>
<u><b>Requirements</b></u>
<br />
<ul>
<li>Web Server: Any server supporting the required PHP version (<a href='http://www.apache.org' target='_blank' title='Apache'>Apache</a> highly recommended)</li>
<li><a href='http://www.php.net' target='_blank' title='PHP'>PHP</a>: Any PHP version >= 5.5 </li>
<li><a href='http://www.mysql.com' target='_blank' title='MySQL'>MySQL</a>: MySQL server 5.0+</li>
</ul>
<br />
<u><b>Before you install</b></u>
<ul>
<li>Setup WWW server, PHP and database server properly.</li>
<li>Prepare a database for your vtCMS site.</li>
<li>Prepare user account and grant the user the access to the database.</li>
<li>Make the directories of uploads/, cache/ and templates_c/ and the files of mainfile.php writabale.</li>
<li>Turn cookie and JavaScript of your browser on.</li>
</ul>
<u><b>Installation</b></u>
<p>
Follow this install wizard.
</p>